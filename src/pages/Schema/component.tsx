import React, { useEffect } from 'react'
import { observer, useField } from '@formily/react';
import { ArrayField as ArrayFieldType } from '@formily/core';
import { Button } from 'antd'

export default observer((props) => {

    const field = useField<ArrayFieldType>();
    useEffect(() => {
        console.log('props', props)
    }, [props])
    const click = ()=>{
        console.log(props)
        field.setValue('自定义组件值')
    }
    return (
        <Button onClick={click}>自定义组件</Button>
    )
})